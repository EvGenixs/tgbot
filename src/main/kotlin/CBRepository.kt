import Data.Api
import Models.Answer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.await


class CBRepository(private val api: Api) {
    suspend fun getCBData(): Answer{
        return withContext(Dispatchers.IO){
            api.getData()
        }.await()
    }
}