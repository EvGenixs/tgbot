fun main() {
    //Создаем ретрофит клиент
    val retclient = RetrofitClient().getRetro(RetrofitType.CB)
    //Создаем апи клиенту
    val api = RetrofitClient().getCBApi(retclient)
    //Создаем репозиторий с апи для передачи репозитория боту
    val reposit = CBRepository(api)
    //Создаем бота
    val bot = TGBot(reposit).createBot()
    //Бот начинает работу
    bot.startPolling()
}