package Data

import Models.Answer
import retrofit2.Call
import retrofit2.http.GET

interface Api {
    @GET("daily_json.js")
    fun getData(): Call<Answer>
}