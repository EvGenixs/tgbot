import Data.Api
import Models.ValuteData
import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.Dispatcher
import com.github.kotlintelegrambot.dispatcher.callbackQuery
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.dispatcher.message
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.InlineKeyboardMarkup
import com.github.kotlintelegrambot.entities.keyboard.InlineKeyboardButton
import com.github.kotlintelegrambot.extensions.filters.Filter
import com.github.kotlintelegrambot.logging.LogLevel

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


private const val IS_DAY = 1
var mapInttoValuteData: Map<Int, ValuteData> = mapOf()
private const val GIF_WAITING_URL =
    "https://tenor.com/view/why-am-i-still-waiting-patiently-waiting-waiting-gif-12710222"
private const val BOT_TOKEN = "5528044749:AAH0eDcSlS_PJtnX9hZp1JqJ2R2XWVRBqk8"
private const val TIMEOUT_TIME = 30

class TGBot (private val cbRepository: CBRepository){

    private var _chatId: ChatId? = null
    private val chatId by lazy { requireNotNull(_chatId) }
    fun createBot(): Bot {
        return bot {
            timeout = TIMEOUT_TIME
            token = BOT_TOKEN
            logLevel = LogLevel.Error
            dispatch {
                setUpCommands()
                setUpCallbacks()
            }
        }
    }

    private fun Dispatcher.setUpCallbacks() {
        callbackQuery(callbackData = "all"){
            getCBValutes()
            message(Filter.Text){
                /*for(i in listOf(data.Valute)){
                    bot.sendMessage(chatId,"""
                        Название: ${data.Valute.AMD.Name}
                        Значение: ${data.Valute.AMD.Value}
                    """.trimIndent())

                }*/
            }
        }
    }

    private fun getCBValutes(): Any {
        CoroutineScope(Dispatchers.IO).launch {
            val data = cbRepository.getCBData()
            mapInttoValuteData = mapOf(
                0 to data.Valute.AUD,
                1 to data.Valute.AZN,
                2 to data.Valute.GBP,
                3 to data.Valute.AMD,
                4 to data.Valute.BGN,
                5 to data.Valute.BRL,
                6 to data.Valute.BYN,
                7 to data.Valute.CAD,
                8 to data.Valute.CHF,
                9 to data.Valute.CNY,
                10 to data.Valute.CZK,
                11 to data.Valute.DKK,
                12 to data.Valute.EUR,
                13 to data.Valute.HKD,
                14 to data.Valute.HUF,
                15 to data.Valute.INR,
                16 to data.Valute.JPY,
                17 to data.Valute.KGS,
                18 to data.Valute.KRW,
                19 to data.Valute.KZT,
                20 to data.Valute.MDL,
                21 to data.Valute.NOK,
                22 to data.Valute.PLN,
                23 to data.Valute.RON,
                24 to data.Valute.SEK,
                25 to data.Valute.SGD,
                26 to data.Valute.TJS,
                27 to data.Valute.TMT,
                28 to data.Valute.TRY,
                29 to data.Valute.UAH,
                30 to data.Valute.USD,
                31 to data.Valute.UZS,
                32 to data.Valute.XDR,
                33 to data.Valute.ZAR
            )
        }

        return mapInttoValuteData.toList()
    }


    private fun Dispatcher.setUpCommands(){
        command("start"){
            _chatId = ChatId.fromId(message.chat.id)
            bot.sendMessage(
                chatId = chatId,
                text = "Привет! Я бот, который знает все курсы валют!\n(По данным ЦБ РФ) \nНапиши /valutes"
            )
        }
        command("valutes"){
            val inlineKeyboardMarkup = InlineKeyboardMarkup.create(
                listOf(
                    InlineKeyboardButton.CallbackData(
                        text = "Все валюты",
                        callbackData = "all"
                    )
                ),
                listOf(
                    InlineKeyboardButton.CallbackData(
                        text = "Определенную валюту",
                        callbackData = "onevalute"
                    )
                )

            )

            bot.sendMessage(
                chatId = chatId,
                text = "Выбери, что ты хочешь узнать! ",
                replyMarkup = inlineKeyboardMarkup
            )
        }

    }
}