import Data.Api
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

const val CB_BASE_URL = "https://www.cbr-xml-daily.ru/"

enum class RetrofitType(val baseUrl: String){
    CB(CB_BASE_URL)
}
class RetrofitClient {
    fun getRetro(retrofitType: RetrofitType): Retrofit{
        return Retrofit.Builder()
            .baseUrl(retrofitType.baseUrl)
            .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    fun getCBApi(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }
    fun getClient(): OkHttpClient{
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(logging)
        return okHttpClient.build()
    }

}